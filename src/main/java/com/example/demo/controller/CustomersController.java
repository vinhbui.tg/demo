package com.example.demo.controller;

import com.example.demo.service.CustomersService;
import com.example.demo.entity.CustomersEntity;

import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
=======
import org.springframework.http.ResponseEntity;
>>>>>>> 203dd474c53ca5c19b1863bd93a4b601066cf235
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class CustomersController {

  @Autowired
  private CustomersService customersService;

  @GetMapping("/customers")
  public List<CustomersEntity> getAllCustomers() {
    return customersService.getAllCustomer();
  }

  @GetMapping("/customers/{id}")
  public Optional<CustomersEntity> getCustomerById(int id) {
    return customersService.findCustomerById(id);
  }

}
